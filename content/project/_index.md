+++
title = "Proyecto docente"
weight = 50
pre = "<i class='fa fa-book'></i> "
+++

{{< button href="../docs/project.pdf" align="center" >}} Documento oficial {{< /button >}}

### ¿Qué vamos a estudiar?

La asigantura consta de cuatro temas:

* Conjuntos.

* Grupos.

* Números enteros.

* Polinomios.

En [este enlace](https://rodas5.us.es/items/1141d30f-73ed-4c7a-92f9-d5046dbdffe1/1/) puedes encontrar las listas de problemas que trabajaremos y otros recursos útiles.


### ¿Cómo vamos a evaluar?

La asignatura se puede superar por evaluación continua, sin necesidad de realizar un examen final. Para ello se llevarán a cabo dos pruebas escritas, cada una de dos horas de duración, en horario de clases, en las fechas siguientes:


| Grupo | Primera prueba           | Segunda prueba                    |
|-------|--------------------------|-----------------------------------|
| A     | Miércoles 6 de noviembre | Viernes 17 de enero               |
| B     | Lunes 4 de noviembre     | Viernes 17 de enero               |
| C     | Martes 5 de noviembre    | Jueves 16 de enero                |
| D     | Martes 5 de noviembre    | Miércoles 15 de enero             |
| E     | Martes 5 de noviembre    | Jueves 16 de enero                |
| F     | Lunes 4 de noviembre     | Miércoles 15 y jueves 16 de enero |

Las pruebas parciales constaran de dos ejercicios, cada uno de los cuales versará sobre un tema diferente. Para aprobar la evaluación continua será necesario todo lo siguiente:

* Obtener al menos un 30% de la calificación en cada ejercicio de cada prueba.

* Que la suma del 40% de la nota de la pimera prueba más el 60% de la segunda sea mayor o igual que 5.

Si se dan ambas condiciones, la nota aprobada de la evaluación continua será la suma del segundo apartado. 

Quienes no superen la evaluación continua podrán presentarse a las convocatorias oficiales en fecha y lugar determinados por la Facultad de Matemáticas. Estos exámenes constarán de cuatro ejercicios, uno por cada tema. Para superarlos será también necesario obtener al menos un 30% de la calificación en cada ejercicio.

Quienes hayan aprobado una de las dos pruebas de evaluación continua podrán optar, solo en la primera convocatoria, a examinarse de los dos ejercicios correspondientes a los temas de la prueba no superada. En este caso, para el cálculo de la nota final se aplicará la misma ponderación que para la evaluación continua.

A la primera convocatoria también se podrán presentar quienes, habiendo aprobado la evaluación continua, deseen subir su nota. Quienes opten por esta vía no corren el riesgo de bajar su nota. Para subirla será necesario realizar el examen completo y obtener al menos un 30% de la calificación en cada ejercicio. 

En cada prueba o examen, los cálculos sin explicaciones que los justifiquen no se considerarán respuestas correctas. La mala presentación y las faltas de ortografı́a tendrán una calificación negativa.


### Clases

Cuatro horas semanales, de las cuales una hora será de problemas, de media a lo largo del semestre.

### Tutorías

| Profesor(a)                         | Días y horas                                                                            | Correo               |
|-------------------------------------|-----------------------------------------------------------------------------------------|----------------------|
| Fernando Muro                       | Martes de 10:30 a 12:30 y jueves y viernes de 9:45 a 11:30                              | <fmuro@us.es>        |
| Luis Narváez                        | Lunes y miércoles de 15:00 a 18:00                                                      | <narvaez@us.es>      |
| Miguel Ángel Olalla (coordinador)   | Lunes y miércoles de 9:00 a 11:00 y jueves de 15:30 a 17:30                             | <miguelolalla@us.es> |
| Mercedes Rosas                      | Martes y jueves de 11:00 a 12:00 y miércoles de 11:00 a 13:00 y de 14:00 a 16:00        | <mrosas@us.es>       |